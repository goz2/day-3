## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective):
Today we focused on Java streams and object-oriented programming (OOP). We learned about the concept of streams in Java and how they can be used to perform various operations on collections of data. We also reviewed the principles and features of OOP, such as encapsulation, inheritance, and polymorphism. Throughout the class, we worked on exercises and examples to understand the practical implementation of these concepts.

- R (Reflective):
Understand.

- I (Interpretive):
Today's class was quite enlightening. I found the Java stream concept to be particularly interesting. It provides a concise and expressive way to process and manipulate collections of data. The ability to chain multiple operations together and perform them on a stream of data is powerful and can greatly simplify code. I also enjoyed revisiting object-oriented programming principles. It reinforced the importance of organizing code in a modular and reusable manner, promoting code readability and maintainability.

- D (Decisional):
I am excited to apply what I have learned today in my future Java programming projects. The knowledge of Java streams will be valuable in situations where I need to perform complex operations on collections of data, such as filtering, mapping, and reducing. By leveraging streams, I can write more concise and efficient code. Additionally, I will strive to incorporate the principles of object-oriented programming into my projects, making use of encapsulation, inheritance, and polymorphism to create modular and extensible code. 
