package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Teacher extends Person implements Observer {
    private int id;
    private String name;
    private int age;

    private List<Klass> klassList;

    public Teacher(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    public List<Klass> getKlassList() {
        return klassList;
    }

    public void setKlassList(List<Klass> klassList) {
        this.klassList = klassList;
    }

    public String introduce() {
        if (this.klassList == null || this.klassList.size() == 0) {
            return String.format("My name is %s. I am %d years old. I am a teacher.", this.name, this.age);
        }
        return String.format("My name is %s. I am %d years old. I am a teacher. I teach Class %s.", this.name, this.age,
                this.klassList.stream().map(klass -> String.valueOf(klass.getNumber())).collect(Collectors.joining(", ")));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Teacher teacher = (Teacher) o;
        return id == teacher.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id);
    }

    public void assignTo(Klass klass) {
        if (this.klassList == null) {
            this.klassList = new ArrayList<>();
        }
        this.klassList.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klassList != null && this.klassList.contains(klass);
    }

    public boolean isTeaching(Student tom) {
        return this.klassList != null && this.klassList.size() > 0 && this.klassList.contains(tom.getKlass());
    }

    @Override
    public void update(String message) {
        System.out.printf("I am %s, teacher of Class ", this.getName());
        System.out.println(message);
    }
}
