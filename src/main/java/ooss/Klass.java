package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;

    private Student leader;
    private final List<Observer> observers;

    public Klass(int number) {
        this.number = number;
        this.observers = new ArrayList<>();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student student) {
        if (student.getKlass() != null && student.getKlass().getNumber() == this.getNumber()) {
            this.setLeader(student);
            System.out.println(student.introduce());
            notifyObservers(String.format("%d. I know %s become Leader.", this.getNumber(), this.leader.getName()));
            return;
        }
        System.out.println("It is not one of us.");
    }

    public boolean isLeader(Student student) {
        return this.leader != null && student == this.leader;
    }
    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void detach(Observer observer) {
        observers.remove(observer);
    }

    public void notifyObservers(String message) {
        for (Observer observer : observers) {
            observer.update(message);
        }
    }
}
